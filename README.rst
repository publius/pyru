Pyru
===================
This is a Python client library for the Roundup Checkout REST API service. It contains a Service class for making signed HTTP requests to Roundup servers and some high level model classes for Consumers, Charities, etc.
