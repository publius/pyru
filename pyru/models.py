#!/usr/bin/env python
# module: models

import hashlib
from uuid import UUID, uuid5
from service import HTTP

###############################################################################
class ModelType(type):
  
  def __new__(__type__, name, bases, __dict__):
    cls = type.__new__(__type__, name, bases, __dict__)
    bucket = __dict__.get('bucket')
    if bucket:
      cls._bucket_uuid = UUID(hashlib.sha256(bucket).hexdigest()[:32])
      cls.uuid = lambda self, s: uuid5(self._bucket_uuid, s).hex
      cls._bucket = bucket
    return cls
 
    

###############################################################################
class Model(object):
  
  __metaclass__ = ModelType
  
  def __init__(self, service, key):
    self.service = service
    self._uri = self._bucket.rstrip('/') + '/' + key
    self._key = key
  
  def __str__(self):
    return self._key

  @property
  def key(self):
    '''@returbucket: the model id
    '''
    return self._key

  @key.setter
  def key(self, key):
    self._uri = self._bucket.rstrip('/') + '/' + key 
    self._key = key 


###############################################################################
class Charity(Model):
  
  bucket = 'charities'

  def __init__(self, service, key):
    Model.__init__(self, service, key)
  

###############################################################################
class Consumer(Model):
  
  bucket = 'consumers'

  def __init__(self, service, fname, lname, email):
    Model.__init__(self, service, self.uuid((fname+lname+email).lower()))
  
  @HTTP('POST')
  def donate(self, charity, amount, async=False):
    '''
    - Record funds trabucketfered from cobucketumer in his balance;
    - Adds funds into charity account.

    args:
      - charity: The key of a target charity for donation.
      - amount: The amount of money to donate.
      - async: True => spawns request on daemon thread, returning an
        AsyncResponse object.
    '''
    return dict(
      bucket='consumers',
      key=self.key,
      event='donate',
      async=async,
      data=dict(
        amt=amount,
        charity=charity.key
      )
    )
    
  

