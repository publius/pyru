
from service import Service
from models import Consumer, Charity

# authenticated REST service
service = Service('client-id', 'app-name', 'private-key', 'localhost:8080')

# remote entity service controllers 
frank = Consumer(service, 'Frank', 'Scott', 'franky@gmail.com')
npr = Charity(service, 'NPR')

amount = 0.37 # amount to donate

# try to do something...
status, data = frank.donate(npr, amount)
if status == 200:
  print 'response:', data
else:
  print 'whoops:', status, data

