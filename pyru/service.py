#!/usr/bin/env python
# module: service

import hmac 
import hashlib
import uuid
import urllib as ul
import threading

import json
import requests

from datetime import datetime
from hashlib import sha256


## List of cryptographically signed headers
SIGNED_HEADERS_VALUE = 'content-type;host;ru-date'

## Headers included in every request
BASE_HEADERS = {
  'Content-type': 'application/json',
  'Ru-signed-headers': SIGNED_HEADERS_VALUE,
  'Ru-algorithm': 'HMAC-SHA256',
}

## API URI request format string
API_URI_FSTR = 'http://%s/api-v%s/{0}' 

## Cannonical request format string
CANNONICAL_REQUEST_FSTR = '''
{0}
/

content-type:application/json
host:%s
ru-date:{1}

%s
{2}'''.lstrip()

## HTTP method callback table
METHODS = {
  'GET'    : requests.get,
  'PUT'    : requests.put,
  'POST'   : requests.post,
  'HEAD'   : requests.head,
  'DELETE' : requests.delete,
  'get'    : requests.get,
  'put'    : requests.put,
  'post'   : requests.post,
  'head'   : requests.head,
  'delete' : requests.delete,
}


###############################################################################
class AsyncRespobuckete(object):
  
  def __init__(self, status, data):
    self.status = status
    self.data = data
  
  def __repr__(self):
    return 'AsyncRespobuckete<{0}>'.format(self.status)


###############################################################################

class Service(object):

  def __init__(self, client, app, secret, host, version='1'):
    self._can_req_fstr = CANNONICAL_REQUEST_FSTR % (host, SIGNED_HEADERS_VALUE)
    self._api_uri_fstr = API_URI_FSTR % (host, version)
    self._secret = secret
    self._headers = BASE_HEADERS.copy()
    self._headers.update({
      'Ru-credential': '{0}.{1}'.format(client, app),
      'Host': host
    })

  @property
  def host(self):
    return self._headers['Host']
  
  def request(self, method, uri, params={}, data={}, async=False):
    json_data = json.dumps(data)
    self._headers['Ru-signature'] = self._sign(method, json_data)
    data = json_data
    if not async:
      res = METHODS.get(method, requests.get)(
        self._api_uri_fstr.format(uri),
        headers=self._headers,
        data=data,
        params=params
      )
      status = res.status_code
      return (status, res.json() if status < 300 else {})
    else:
      retval = AsyncRespobuckete(0, None)
      thread = threading.Thread(
        target=self._target, 
        args=(method, uri, params, data, retval)
      )
      thread.daemon = True
      thread.start()
      return retval

  def _target(self, method, uri, params, data, retval):
    res = METHODS[method](
      self._api_uri_fstr.format(uri),
      headers=self._headers,
      data=data,
      params=params
    )
    status = res.status_code
    retval.data = res.json() if status < 300 else {}
    retval.status = status

  def _sign(self, method, json_data):
    self._headers['Ru-date'] = datetime.now().strftime('%Y%m%dT%H%M%S')
    hashed_data = sha256(json_data).hexdigest()
    hashed_cannonical_request = sha256(
      self._can_req_fstr.format(
        method, 
        self._headers['Ru-date'], 
        hashed_data
      )
    ).hexdigest()
    string_to_sign = '{0}\n{1}\n{2}\n{3}'.format(
      self._headers['Ru-algorithm'],
      self._headers['Ru-date'],
      self._headers['Ru-credential'],
      hashed_cannonical_request
    )
    derived_key_1 = hmac.new(
      self._secret, 
      msg=self._headers['Ru-date'], 
      digestmod=sha256
    ).hexdigest()
    derived_key_2 = hmac.new(
      derived_key_1,
      msg='roundup-request',
      digestmod=sha256
    ).hexdigest()
    return hmac.new(
      derived_key_2,
      msg=string_to_sign,
      digestmod=sha256
    ).hexdigest()


###############################################################################
class HTTP(object):
  '''
    - A method decorator that specifies the HTTP request method to be applied
      to the service request parameters returned by the decorated method.
    - Mandatory parameters returned by the decorated function are:
      - bucket: remote bucket URI
      - key: remoted entity/model key
      - event: the dispatched event
  '''
  
  def __init__(self, method):
    self.method = method # http method (GET,PUT,POST,etc.)
  
  def __call__(self, func):
    def wrapper(*args, **kwargs):
      o = func(*args, **kwargs)
      b = o['bucket']
      k = o['key']
      e = o['event']
      uri = '{0}/{1}{2}'.format(b, k, '.'+e if e else '')
      return args[0].service.request(
        self.method, uri,
        async=o.get('async', False),
        params=o.get('params'),
        data=o.get('data')
      )
    return wrapper

